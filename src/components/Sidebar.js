import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

export class Sidebar extends Component {
  render() {
    return (
      <div id="sidebar">
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/page-one/">Page One</Link>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Sidebar
