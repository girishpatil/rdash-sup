import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import logo from './logo.svg';
import './App.css';

// Import pages
import Index from './pages/Index';
import PageOne from './pages/PageOne';

// Import components
import Sidebar from './components/Sidebar';

function App() {
  return (
    <Router>
      <div id="main-container">
        <Sidebar />
        <div id="container">
          <Route path="/" exact component={Index} />
          <Route path="/page-one/" component={PageOne} />
        </div>
      </div>
    </Router>
  );
}

export default App;
